import java.util.Stack;

public class ArrayStack<T> extends Stack<T> {
        private final int DEFAULT_CAPACITY = 10;
        private int count;
        private T[] stack;

        public ArrayStack()
        {
            count = 0;
            stack = (T[]) (new Object[DEFAULT_CAPACITY]);
        }

        public T push (T element)
        {
            if (count == stack.length)
                expandCapacity();
            stack[count] = element;
            count++;
            return element;
        }

        public String toString()
        {
            String result = "<top of stack>\n";
            for (int index=count-1; index >= 0; index--)
            result += stack[index] + "\n";
            return result + "<bottom of stack>";
        }

        private void expandCapacity()
        {
            T[] larger = (T[])(new Object[stack.length*2]);
            for (int index=0; index < stack.length; index++)
                larger[index] = stack[index];
            stack = larger;
        }

        public T pop (){

        }
        public T peek ()
        {
            return stack[stack.length-1];
        }

        public boolean isEmpty() 
        {
            if(stack[0] == null){
                return true;
            }
            else{
                return false;
            }
        }

        public int size() 
        {
            int size;
            size = stack.length;
            return size;   
        }
}
