Assignment 2 Instructions and Submission
According to our suggested 13 week course schedule, this programming project should be completed and submitted by the Monday of Week 6. It is worth 8% of your final grade. Please refer to the “Assignments Overview” for details on the marking rubric and submission of work.

PP 12.4 (page 515)

The ArrayStack implementation in this chapter keeps the top variable pointing to the next array position above the actual top of the stack. Modify the array implementation such that stack[top] is the actual top of the stack.

PP 13.8 (page 546)

There is a data structure called a drop-out stack that behaves like a stack in every respect except that if the stack size is n, then when the n+1 element is pushed, the bottom element is lost. Implement a drop-out stack using links, by modifying the LinkedStack code.

PP 14.6 (page 578)

A data structure called a deque (pronounced like “deck”) is closely related to a queue. The name deque stands for double-ended queue. The difference between the two is that with a deque you can insert or remove from either end of the queue. Implement a deque using arrays (start with CircularArrayQueue).

PP 14.9 (page 579)

Create an application using both a stack and a queue to test whether a given string is a palindrome (i.e., the characters read the same forward or backward). Your algorithm should parse the input string only ONCE, and not construct a reverse string.